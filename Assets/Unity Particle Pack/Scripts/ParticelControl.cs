﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticelControl : MonoBehaviour {
    ParticleSystem[] ps;

	// Use this for initialization
	void Start () {
        ps = GetComponents<ParticleSystem>();	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.F))
        {
            foreach(ParticleSystem pso in ps)
                pso.Play();
        }
            
	}
}
