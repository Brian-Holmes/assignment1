﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityScript.Steps;

public class Grenade : MonoBehaviour
{
    public float delay = 5f;

    float countdown;
    bool hasExploded = false;

    public GameObject explosionEffect;
    private AudioSource grenadeAudio;

    public float range = 50f;
    public float up = 4f;
    public float explosionForce = 2000f;

    void Start()
    {
        countdown = delay;
        grenadeAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0f  && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }

    void Explode() 
    {
        Debug.Log("Boom");

        // Show Explosion

        Instantiate(explosionEffect, transform.position, transform.rotation);

        // Hear Explosion
        grenadeAudio.PlayOneShot(grenadeAudio.clip);

        // Explosion Force
        Vector3 explosionPos = transform.position;

        Collider[] colliders = Physics.OverlapSphere(explosionPos, range);

        foreach (Collider hit in colliders)
        {
            if (hit.GetComponent<Rigidbody>())
                hit.GetComponent<Rigidbody>().AddExplosionForce(explosionForce, explosionPos, range, up);
        }
      
    }

}
