﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    private AudioSource playerAudio;
    public AudioClip gunshot;
    public AudioClip footstep;


    void Awake()
    {
        playerAudio = GetComponent<AudioSource>();
    }


    public void Gunshot() 
    {
        playerAudio.PlayOneShot(gunshot, 0.8f);
    }

    public void Footstep()
    {
        playerAudio.PlayOneShot(footstep, 0.8f);
    }


}
