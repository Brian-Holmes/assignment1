﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorHinge : MonoBehaviour
{
    private Animator _animator = null;
    
    void Start()
    {
        _animator = GetComponentInChildren<Animator>();    
    }
    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            _animator.SetBool("isOpen", true);
        }
        
    }

    void OnTriggerExit(Collider collider) 
    {
        _animator.SetBool("isOpen", false);
    }
}
