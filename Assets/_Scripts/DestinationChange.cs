﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;


/// <summary>
/// This scrpit will be attached to multiple trigger points and will tell a colliding "pedestrian" go back to where it started.
/// </summary>
public class DestinationChange : MonoBehaviour 
{
    public GameObject[] npcPedestrians; // stores a list 
    public Dictionary<string, Vector3> npcStart = new Dictionary<string, Vector3>();  // stores each "Pedestrian" along with their starting position
    public Vector3 firstTrigger;
    //  *************** public Vector3 npc1Start;

    // Start is called before the first frame update
    void Start()
    {
        firstTrigger = this.transform.position;

        npcPedestrians = GameObject.FindGameObjectsWithTag("Pedestrian"); // populates an array of all objects with "Pedestrian" tag 
        

        // ========== DEBUGGING ===========
        foreach(GameObject npc in npcPedestrians)
        {
            Debug.Log("This npc is: " + npc.name);
        }

        
        foreach (GameObject npc in npcPedestrians) //iterates through the list of npcs with the tag "Pedestrian"
        {
            Debug.Log("Adding :" + npc.name + ", " + npc.transform.position + " to dictionary");
            npcStart.Add(npc.name, npc.transform.position); // creates dictionary entry storing npc's name, and starting location.
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggered by: " + other.name);

        Vector3 npcStartLocation; // a variable to store the value of the npc's starting point
        Vector3 newStart; // A variable to store the destination as a new 'starting point'
                
        if (npcStart.TryGetValue(other.gameObject.name, out npcStartLocation)) // finds the npc's starting position by searching dictionary for it's name
        {
            Debug.Log(other.gameObject.name + ", " + npcStartLocation);
            newStart = this.gameObject.transform.position; // stores the location of the trigger point as the new start
            this.gameObject.transform.position = npcStartLocation; // moves the trigger point the the npc's starting location
            npcStart[other.gameObject.name] = newStart; // applies the change to the npc's starting point to the dictionary reference for that npc.
            Debug.Log(other.gameObject.name +  " destination set to: " + newStart);
        }

    }


}
