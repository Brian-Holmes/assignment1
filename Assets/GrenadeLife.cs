﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLife : MonoBehaviour
{
    void Update()
    {
        StartCoroutine(Destroy());
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(3);

        Destroy(this);

    }

}
